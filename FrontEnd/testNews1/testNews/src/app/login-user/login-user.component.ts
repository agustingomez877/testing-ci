import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { UserRegisterComponent} from '../user-register/user-register.component' 
import { MatDialog } from '@angular/material/dialog';
import { NewsServiceService } from '../newsService.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-login-user',
  templateUrl: './login-user.component.html',
  styleUrls: ['./login-user.component.css']
})
export class LoginUserComponent {
  username: string = '';
  password: string = '';

  constructor(
    private dialogRef: MatDialogRef<LoginUserComponent>,
    private dialog: MatDialog,
    private newService: NewsServiceService) {}

  async login() {
   let request =
   {
    "firstName": this.username,
    "contrasena": this.password,
    
   }  
  
   if (request.firstName === '' || request.contrasena === '') {
    
    Swal.fire({
      icon: 'error',
      title: 'Campos vacíos',
      text: 'Por favor, completa todos los campos.'
    });
  } else {
    let responseRegisterUser = await this.newService.postRegisterUser(request).toPromise();
    console.log(responseRegisterUser);
   
  }
    this.dialogRef.close();
  }

  registerUser()
  {
    const dialogReg = this.dialog.open(UserRegisterComponent)
  }
}

