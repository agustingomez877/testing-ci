import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { NewsServiceService } from '../newsService.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent {
  nombre: string = '';
  apellido: string = '';
  correo: string = '';
  contrasena: string = '';

  constructor(private dialogReg: MatDialogRef<UserRegisterComponent>,private newService: NewsServiceService,) {}

 async registrar() {

  let request = 
  {
    "firstName": this.nombre,
    "lastName": this.apellido,
    "email": this.correo,
    "contraseña": this.contrasena
  }
  console.log(request)
  if (request.firstName === '' || request.lastName === '' || request.email === '' || request.contraseña === '') {
    
    Swal.fire({
      icon: 'error',
      title: 'Campos vacíos',
      text: 'Por favor, completa todos los campos.'
    });
  } else {
    let responseRegisterUser = await this.newService.postRegisterUser(request).toPromise();
    console.log(responseRegisterUser);
    this.dialogReg.close();
  }
    this.dialogReg.close();
  }
}
