import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { FavoritosPageComponent } from './favoritos-page/favoritos-page.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'favoritos', component: FavoritosPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
