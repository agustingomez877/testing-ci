import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from 'src/environment/environment.dev';



const cabecera = {headers: new HttpHeaders({'Content-Type': 'application/json'})};
@Injectable()
export class NewsServiceService {
  url: any;

  constructor(private http: HttpClient) { }

  getNewsService() {
   let url= environment.noticiasAPI;
   return this.http.get<any>(url).toPromise();
  }
  getAllusers() {
    let url= environment.User;
    return this.http.get<any>(url).toPromise();
   }
   getusers() {
    let url= environment.User +'id';
    return this.http.get<any>(url).toPromise();
   }
   getAllfavorites() {
    let url= environment.Favorites;
    return this.http.get<any>(url).toPromise();
   }
   getfavorites() {
    let url= environment.Favorites +'id';
    return this.http.get<any>(url).toPromise();
   }

   postRegisterUser(User: any) {
   let url= environment.User;
   return this.http.post<any>(url, JSON.stringify(User), cabecera)
   }
   
   postRegisterFavorites(Favorites: any) {
    let url= environment.Favorites;
    return this.http.post<any>(url, JSON.stringify(Favorites), cabecera)
    }
 


}
