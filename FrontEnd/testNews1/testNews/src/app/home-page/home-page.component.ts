import { Component, OnInit, ViewChild } from '@angular/core';
import { NewsServiceService } from '../newsService.service';
import { DatePipe } from '@angular/common';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { LoginUserComponent } from '../login-user/login-user.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  respuestaNoticias: any[] = [];
  filteredNoticias: any[] = []; 
  pageSize = 10;
  totalItems = 0;
  searchText = '';

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private newService: NewsServiceService,
    private datePipe: DatePipe,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.getNewsService();
  }

  async getNewsService() {
    let response = await this.newService.getNewsService();
    console.log(response);
    this.respuestaNoticias = response.results;
    this.totalItems = this.respuestaNoticias.length;
    this.filteredNoticias = this.respuestaNoticias; 
    console.log(this.respuestaNoticias);
  }

  formatDate(date: string): string {
    return this.datePipe.transform(date, 'dd MMM yyyy') || '';
  }

  onPageChange(event: PageEvent) {
    const startIndex = event.pageIndex * event.pageSize;
    const endIndex = startIndex + event.pageSize;
    this.filteredNoticias = this.respuestaNoticias.slice(startIndex, endIndex);
  }

  searchNews() {
    
    this.filteredNoticias = this.respuestaNoticias.filter(noticia =>
      noticia.title.toLowerCase().includes(this.searchText.toLowerCase())
    );
  }

  openLoginDialog() {
    const dialogRef = this.dialog.open(LoginUserComponent);
  }
  
}
